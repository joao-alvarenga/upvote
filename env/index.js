import axios from "axios";

const baseURL = process.env.API;

const api = axios.create({
  baseURL,
  headers: {
    Accept: "application/json",
  },
});

export default baseURL;
