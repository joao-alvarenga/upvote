import React from "react";
import AuthForm from "../../components/authForm/AuthForm";
import AuthButton from "../../components/button/AuthButton";
import Input from "../../components/input/Input";
import LinkText from "../../components/input/LinkText";
import Logo from "../../components/logo/Logo";
import "./style.scss";
import { LinksWrap, LoginSection } from "./loginElements";

const LoginPage = () => {
  return (
    <LoginSection className="login__page">
      <Logo />
      <AuthForm>
        <Input
          labelName="Nome de usuário"
          htmlFor="username"
          type="text"
          id="username"
        />
        <Input
          type="password"
          labelName="Senha"
          htmlFor="password"
          id="password"
        />
        <AuthButton children="Entrar" />
        <LinksWrap className="auth__links">
          <p>Esqueceu a senha?</p>
          <LinkText to="/password-recovery">Redefinir senha</LinkText>
          <p className="registration__link">Não tem uma conta?</p>
          <LinkText to="/registration">Se-inscreva</LinkText>
        </LinksWrap>
      </AuthForm>
    </LoginSection>
  );
};

export default LoginPage;
