import styled from "styled-components";
import { flexContainer } from "../../styles/styleVariables";

export const LoginSection = styled.section`
  width: 100%;
  height: 100%;
  position: fixed;
  ${flexContainer};
  flex-direction: column;
`;

export const LinksWrap = styled.div`
  display: flex;
  gap: 1rem;
  font-size: 1.4rem;
`;
