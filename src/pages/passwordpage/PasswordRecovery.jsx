import React from "react";
import AuthForm from "../../components/authForm/AuthForm";
import AuthButton from "../../components/button/AuthButton";
import Input from "../../components/input/Input";
import Logo from "../../components/logo/Logo";
import { PasswordSection } from "./passwordElements.js";

const PasswordPage = () => {
  return (
    <PasswordSection>
      <Logo />
      <AuthForm>
        <Input labelName="Nome de usuário" htmlFor="username" id="username" />
        <AuthButton children="Redefinir senha" />
      </AuthForm>
    </PasswordSection>
  );
};

export default PasswordPage;
