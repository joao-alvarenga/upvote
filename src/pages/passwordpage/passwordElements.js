import styled from "styled-components";
import { flexContainer } from "../../styles/styleVariables";

export const PasswordSection = styled.section`
  width: 100%;
  height: 100%;
  position: fixed;
  ${flexContainer};
  flex-direction: column;
`;
