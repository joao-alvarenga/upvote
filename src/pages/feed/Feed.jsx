import React from "react";
import Navbar from "../../components/navbar/Navbar";
import PostForm from "../../components/newPostForm/PostForm";
import Post from "../../components/posts/Post";
import "./feedElements.scss";

const Feed = () => {
  return (
    <>
      <Navbar />
      <section className="feed">
        <div className="feed__container">
          <PostForm />
          <Post />
        </div>
      </section>
    </>
  );
};

export default Feed;
