import React from "react";
import AuthForm from "../../components/authForm/AuthForm";
import AuthButton from "../../components/button/AuthButton";
import Input from "../../components/input/Input";
import LinkText from "../../components/input/LinkText";
import Logo from "../../components/logo/Logo";
import { RegistrationSection, LinksWrap } from "./registrationElements.js";

const Registration = () => {
  return (
    <RegistrationSection>
      <Logo />
      <AuthForm>
        <Input
          labelName="Nome de usuário"
          htmlFor="username"
          type="text"
          id="username"
        />
        <Input
          labelName="Senha"
          htmlFor="password"
          id="password"
          type="password"
        />
        <AuthButton children="Se-inscreva" />
        <LinksWrap className="login__link">
          <p>Já tem uma conta?</p>
          <LinkText to="/">Entrar</LinkText>
        </LinksWrap>
      </AuthForm>
    </RegistrationSection>
  );
};

export default Registration;
