import styled from "styled-components";
import { flexContainer } from "../../styles/styleVariables.js";

export const RegistrationSection = styled.section`
  width: 100%;
  height: 100%;
  position: fixed;
  ${flexContainer};
  flex-direction: column;
`;

export const LinksWrap = styled.div`
  display: flex;
  justify-content: space-between;
  font-size: 1.4rem;
`;
