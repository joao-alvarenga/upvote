import styled from "styled-components";
import { flexContainer } from "../../styles/styleVariables";

export const AuthWrap = styled.div`
  width: 50%;
  height: 50%;
  ${flexContainer()}
  background-color: #eee;
`;

export const AuthContainer = styled.div`
  width: 80%;
  display: flex;
  justify-content: center;
  flex-direction: column;
  gap: 2rem;
`;
