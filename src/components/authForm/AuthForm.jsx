import React from "react";
import { AuthWrap, AuthContainer } from "./authFormElements";

const AuthForm = ({ children }) => {
  return (
    <AuthWrap className="formAuth">
      <AuthContainer className="formAuth__container">{children}</AuthContainer>
    </AuthWrap>
  );
};

export default AuthForm;
