import React from "react";
import { LogoWrap, LogoContent, LogoSpan } from "./logoElements";

const Logo = ({ classNames, spanClass, style, onClick }) => {
  return (
    <LogoWrap>
      <LogoContent className={classNames} style={style} onClick={onClick}>
        <LogoSpan className={spanClass}>Hot</LogoSpan>
        votes
      </LogoContent>
    </LogoWrap>
  );
};

export default Logo;
