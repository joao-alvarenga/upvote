import styled from "styled-components";
import { primaryColor } from "../../styles/colors/colors";

export const LogoWrap = styled.div`
  width: max-content;
  height: max-content;
`;

export const LogoContent = styled.h2`
  font-size: 3rem;
`;

export const LogoSpan = styled.span`
  color: ${primaryColor};
  font-family: "Suez One", sans-serif;
`;
