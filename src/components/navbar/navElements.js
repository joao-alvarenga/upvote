import styled from "styled-components";
import { primaryColor } from "../../styles/colors/colors";
// import { Link as LinkForm } from "react-router-dom";

export const NavbarWrap = styled.header`
  min-height: 5rem;
  width: 100%;
  position: fixed;
  top: 0;
  left: 0;
  z-index: 1;
  background-color: ${primaryColor};
`;

export const NavbarContainer = styled.div``;

export const LabelForm = styled.label`
  font-size: 1.4rem;
`;

// export const Link = styled(LinkForm)`
//   text-decoration: none;
//   color: ${linkColor};
// `;
