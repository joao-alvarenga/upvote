import React from "react";
import Logo from "../logo/Logo";
import { NavbarContainer, NavbarWrap } from "./navElements";
import "./nav.scss";

const Navbar = () => {
  return (
    <NavbarWrap>
      <NavbarContainer>
        <Logo classNames="navLogo" spanClass="spanClass" />
      </NavbarContainer>
    </NavbarWrap>
  );
};

export default Navbar;
