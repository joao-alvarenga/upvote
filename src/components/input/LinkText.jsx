import React from "react";
import { Link } from "./inputElements";

const LinkText = ({ children, style, onChange, to, value, id }) => {
  return (
    <Link
      to={to}
      className="link"
      onChange={onChange}
      value={value}
      style={style}
      id={id}
    >
      {children}
    </Link>
  );
};

export default LinkText;
