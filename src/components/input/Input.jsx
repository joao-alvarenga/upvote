import React from "react";
import { InputWrap, InputForm, LabelForm } from "./inputElements";
const Input = ({
  placeholder,
  label,
  name,
  labelName,
  htmlFor,
  type,
  className,
  labelClass,
  onChange,
  value,
  id,
}) => {
  return (
    <InputWrap className="input__wrap">
      <LabelForm htmlFor={htmlFor} className={labelClass}>
        {labelName}
      </LabelForm>
      <InputForm
        className={className}
        placeholder={placeholder}
        label={label}
        name={name}
        type={type}
        onChange={onChange}
        value={value}
        id={id}
      />
    </InputWrap>
  );
};

export default Input;
