import styled from "styled-components";
import { linkColor, primaryColor } from "../../styles/colors/colors";
import { Link as LinkForm } from "react-router-dom";

export const InputWrap = styled.div`
  display: flex;
  flex-direction: column;
`;

export const InputForm = styled.input`
  width: 100%;
  height: 3rem;
  padding-left: 3px;
  outline: none;

  &:focus {
    border: 1px solid ${primaryColor};
  }
`;

export const LabelForm = styled.label`
  font-size: 1.4rem;
`;

export const Link = styled(LinkForm)`
  text-decoration: none;
  color: ${linkColor};
`;
