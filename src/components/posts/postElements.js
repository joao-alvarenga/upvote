import styled from "styled-components";
import { primaryColor } from "../../styles/colors/colors";

export const PostSection = styled.section`
  width: 100%;
  height: fit-content;
  display: flex;
  background-color: #fff;
`;

export const PostWrap = styled.div`
  width: 100%;
  height: 11rem;
`;

export const PostContent = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: space-between;
  flex-direction: column;
`;

export const PostUserName = styled.div`
  padding: 1rem 0.5rem;
  font-size: 1.5rem;
  color: ${primaryColor};
`;

export const PostText = styled.div`
  padding: 1rem 0.5rem;
  font-size: 1.7rem;
`;

export const PostReaction = styled.div`
  padding: 1rem 0.5rem;
  font-size: 1.5rem;
  display: flex;
  gap: 1rem;
`;
