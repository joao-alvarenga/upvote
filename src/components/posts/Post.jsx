import React from "react";
import {
  AiOutlineHeart,
  AiFillHeart,
  AiOutlineLike,
  AiFillLike,
} from "react-icons/ai";
import {
  PostContent,
  PostSection,
  PostText,
  PostUserName,
  PostWrap,
  PostReaction,
} from "./postElements";

const Post = () => {
  return (
    <PostSection>
      <PostWrap>
        <PostContent>
          <PostUserName>@UserName</PostUserName>
          <PostText>some bushit that is!</PostText>
          <PostReaction>
            <AiOutlineLike style={{ cursor: "pointer" }} />
            <AiOutlineHeart style={{ cursor: "pointer" }} />
          </PostReaction>
        </PostContent>
      </PostWrap>
    </PostSection>
  );
};

export default Post;
