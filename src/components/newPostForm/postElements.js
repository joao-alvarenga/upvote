import styled from "styled-components";
import { primaryColor } from "../../styles/colors/colors";
// import { flexContainer } from "../../styles/styleVariables";

export const PostContainer = styled.section`
  width: 100%;
  height: 100%;
  margin: 1.2rem 0;
  display: flex;
  align-items: center;
  position: relative;
  background-color: #fff;
`;

export const PostWrap = styled.div`
  min-height: 6rem;
  width: 100%;
`;

export const PostBtn = styled.button`
  width: 13rem;
  height: 3.4rem;
  outline: none;
  border: none;
  position: absolute;
  right: 1rem;
  top: 23%;
  color: #fff;
  cursor: pointer;
  background-color: ${primaryColor};

  &:hover {
    opacity: 0.8;
  }

  &:active {
    opacity: 1;
  }
`;
