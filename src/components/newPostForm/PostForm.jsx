import React from "react";
import Input from "../input/Input";
import { PostBtn, PostWrap, PostContainer } from "./postElements.js";
import "./newPost.scss";

const PostForm = () => {
  return (
    <PostContainer className="filterForm">
      <PostWrap className="postForm__wrap">
        <textarea
          type="text"
          className="postArea"
          placeholder="No que você está pensando?"
        />
        <PostBtn>Postar</PostBtn>
      </PostWrap>
    </PostContainer>
  );
};

export default PostForm;
