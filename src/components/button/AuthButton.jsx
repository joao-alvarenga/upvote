import React from "react";
import { Button } from "./button.js";

const AuthButton = ({ children, style, onClick }) => {
  return (
    <Button style={style} onClick={onClick}>
      {children}
    </Button>
  );
};

export default AuthButton;
