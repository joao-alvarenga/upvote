import styled from "styled-components";
import { primaryColor } from "../../styles/colors/colors";

export const Button = styled.button`
  outline: none;
  border: none;
  color: #fff;
  background-color: ${primaryColor};
  width: 50%;
  height: 4rem;
  font-size: 1.6rem;
  border-radius: 2px;
  cursor: pointer;

  &:hover {
    opacity: 0.8;
  }
`;
