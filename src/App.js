import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import Feed from "./pages/feed/Feed";
import LoginPage from "./pages/loginPage/LoginPage";
import PasswordPage from "./pages/passwordpage/PasswordRecovery";
import Registration from "./pages/registrationPage/Registrtion";

// import scss file
import "./styles/globalStyle.scss";

function App() {
  return (
    <div className="App">
      <Switch>
        <Route
          exact
          path="/"
          render={() => (
            <>
              <LoginPage />
            </>
          )}
        />
        <Route
          path="/registration"
          render={() => (
            <>
              <Registration />
            </>
          )}
        />
        <Route
          path="/password-recovery"
          render={() => (
            <>
              <PasswordPage />
            </>
          )}
        />
        <Route
          path="/feed"
          render={() => (
            <>
              <Feed />
            </>
          )}
        />
      </Switch>
    </div>
  );
}

export default App;
